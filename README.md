# 1000Genome workflow DAG analysis

```
# file tree 
|-- 1000Genome-workflow  	# project repo of 1000Genome-workflow (submodule)
|-- example_sankey		# an example of generated sankey plot
|-- example_yml_logs		# an example of generated yaml files with Pegasus
|-- scripts   
	|-- shell_exe_gen.py	# an script to generate shell execution script from Pegasus yamls
	|-- exe_script.sh	# an example automatically generated workflow execution shell script
	|-- sankey_plot_1000genome.py
				# an script to generate Sankey diagram from Tazer stat log
```

## An example of generated sankey plot
![1000genome_workflow_sankey](example_sankey/sankey.png)
