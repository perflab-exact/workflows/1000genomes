/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/20130502/ALL.chr1.250000.vcf ./
/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/20130502/columns.txt ./
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/individuals.py ALL.chr1.250000.vcf 1 1 2 5 
mkdir individuals_ID0000001
mv *stat individuals_ID0000001/
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/individuals.py ALL.chr1.250000.vcf 1 2 3 5 
mkdir individuals_ID0000002
mv *stat individuals_ID0000002/
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/individuals.py ALL.chr1.250000.vcf 1 3 4 5 
mkdir individuals_ID0000003                        
mv *stat individuals_ID0000003/                     
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/individuals.py ALL.chr1.250000.vcf 1 4 5 5
mkdir individuals_ID0000004/
mv *stat individuals_ID0000004                     
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/individuals_merge.py 1 chr1n-1-2.tar.gz chr1n-2-3.tar.gz chr1n-3-4.tar.gz chr1n-4-5.tar.gz
mkdir individuals_merge_ID0000005
mv *stat individuals_merge_ID0000005/

/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/20130502/ALL.chr1.phase3_shapeit2_mvncall_integrated_v5.20130502.sites.annotation.vcf ./
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/sifting.py ALL.chr1.phase3_shapeit2_mvncall_integrated_v5.20130502.sites.annotation.vcf 1
mkdir sifting_ID0000006
mv *stat sifting_ID0000006

if [ ! -f SAS.txt ]
then
        mv SAS SAS.txt
fi
/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/populations/SAS ./
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/mutation_overlap.py -c 1 -pop SAS.txt
mkdir mutation_overlap_ID0000007
mv *stat mutation_overlap_ID0000007

LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/frequency.py -c 1 -pop SAS.txt
mkdir frequency_ID0000008
mv *stat frequency_ID0000008

if [ ! -f EAS.txt ]
then
        mv EAS EAS.txt
fi
/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/populations/EAS ./
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/mutation_overlap.py -c 1 -pop EAS.txt
mkdir mutation_overlap_ID0000009
mv *stat mutation_overlap_ID0000009

LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/frequency.py -c 1 -pop EAS.txt
mkdir frequency_ID0000010
mv *stat frequency_ID0000010

if [ ! -f GBR.txt ]
then
        mv GBR GBR.txt
fi
/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/populations/GBR ./
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/mutation_overlap.py -c 1 -pop GBR.txt
mkdir mutation_overlap_ID0000011
mv *stat mutation_overlap_ID0000011

LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/frequency.py -c 1 -pop GBR.txt
mkdir frequency_ID0000012
mv *stat frequency_ID0000012

if [ ! -f AMR.txt ]
then
        mv AMR AMR.txt
fi
/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/populations/AMR ./
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/mutation_overlap.py -c 1 -pop AMR.txt
mkdir mutation_overlap_ID0000013
mv *stat mutation_overlap_ID0000013

LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/frequency.py -c 1 -pop AMR.txt
mkdir frequency_ID0000014
mv *stat frequency_ID0000014

if [ ! -f AFR.txt ]
then
        mv AFR AFR.txt
fi
/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/populations/AFR ./
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/mutation_overlap.py -c 1 -pop AFR.txt
mkdir mutation_overlap_ID0000015
mv *stat mutation_overlap_ID0000015

LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/frequency.py -c 1 -pop AFR.txt
mkdir frequency_ID0000016
mv *stat frequency_ID0000016

if [ ! -f EUR.txt ]
then
        mv EUR EUR.txt
fi
/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/populations/EUR ./
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/mutation_overlap.py -c 1 -pop EUR.txt
mkdir mutation_overlap_ID0000017
mv *stat mutation_overlap_ID0000017

LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/frequency.py -c 1 -pop EUR.txt
mkdir frequency_ID0000018
mv *stat frequency_ID0000018

if [ ! -f ALL.txt ]
then
        mv ALL ALL.txt
fi
/bin/cp /qfs/people/guol678/projects/1000genome-workflow//data/populations/ALL ./
LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/mutation_overlap.py -c 1 -pop ALL.txt
mkdir mutation_overlap_ID0000019
mv *stat mutation_overlap_ID0000019

LD_PRELOAD=/people/guol678/software/tazer/lib/libclient.so /qfs/people/guol678/projects/1000genome-workflow//bin/frequency.py -c 1 -pop ALL.txt
mkdir frequency_ID0000020
mv *stat frequency_ID0000020
