# generate Sankey diagram taking Tazer stat files as input
# for every input/output of a given workflow Tazer generates
# two stat files: *_w_stat and *_r_stat for write and read
# counts. This script also takes Pegasus generated yaml files
# as input to figure out input/output automatically while 
# matching with their Tazer stat. 

import plotly.graph_objects as go
import pandas as pd
import pandas as pd
import os
from fnmatch import fnmatch
import yaml

class SankeyData:
    
    # From Tazer
    df_stat = list()
    # Sankey in Plotly, node - vertex, link - edge
    nodes = pd.DataFrame(columns=['label', 'color'])
    links = pd.DataFrame(columns=['source', 'target', 'value'])
    
    color_map = {"task": "red",
                 "file": "blue",
                 "none": "grey"
                }
    block_size = 1024
        
    def __init__(self):
        pass
        
    def load_stat(self, df_stat, params):
        
        self.df_stat.append({ "df": df_stat,
                              "filename": params['filename'],
                              "type": params['type'],
                              "size": params['size'],
                             "task_name": params['task_name']
                            })

        # add a node entry (task)
        self.set_taskname(params['task_name'])

        # add a node entry (input/output file)
        # one filename entry is allowed
        if len(self.nodes[self.nodes.label == params['filename']]) == 0:
            self.add_node({'label': params['filename'],
                           'color': self.color_map['file']})
            '''
            self.add_node({'label': 'no read',
                      'color': self.color_map['none']})
            self.add_node({'label': 'no write',
                      'color': self.color_map['none']})
            '''
        
    def set_taskname(self, name):
        if len(self.nodes[self.nodes.label == name]) == 0:
            self.add_node({'label': name,
                           'color': self.color_map['task']})
                    
    def get_node(self, idx):
        return self.nodes[idx]
    
    def add_node(self, node_dict):
        # label, color, customdata, x, y
        x = pd.DataFrame([node_dict])
        self.nodes = pd.concat([self.nodes, x], ignore_index=True)
        new_id = len(self.nodes)
        
    def get_link(self, name):
        return self.link[name]
    
    def set_links(self):
        pass
    
    def build_links(self, no_rw=False):

        links = []
        key = 'block_idx'
        for v in self.df_stat:
            # input (r)
            cnt = v['df'][key].nunique()
            io_type = v['type']
            fname = v['filename']
            tname = v['task_name']
            fidx = int(self.nodes[self.nodes.label == fname].index.values)
            tidx = int(self.nodes[self.nodes.label == tname].index.values)
            if io_type == "r":
                t2f = {'source': fidx,
                       'target': tidx,
                       'value': cnt}
                if no_rw:
                    t2n = {'source': fidx + 1,
                           'target': tidx,
                           'value': (v['size'] / self.block_size) - cnt}
            else:
                t2f = {'source': tidx,
                       'target': fidx,
                       'value': cnt}
                if no_rw:
                    t2n = {'source': tidx,
                           'target': fidx + 2,
                           'value': (v['size'] / self.block_size) - cnt}

            links.append(t2f) 
            links.append(t2n) if no_rw else None

        links = pd.DataFrame(links)
        self.links = links
    
    def plot(self):
        n = self.nodes[['label','color']].to_dict('list')
        l = self.links.to_dict('list')
        fig = go.Figure(data=[go.Sankey(
            node = n,
            link = l)])
        #fig.show()    
        fig.update_layout(dict1=dict(height=1024, width=1500), title_text="1000genome Sankey Diagram", font_size=10)
        fig.write_image("data_movement.png")

    def reset(self):
        del(self.df_stat)
        del(self.nodes)
        del(self.links)    


# loading tazer stat files into pandas dataframe
def stat_to_df(fname):

    df = pd.read_csv(fname, sep=' ', names=['block_idx','frequency'], skiprows=1)
    return df

if __name__ == "__main__":
    stat_rw_files = {}
    for path, subdirs, files in os.walk(os.getcwd()):
        for name in files:
            if fnmatch(name, "*_r_stat") or fnmatch(name, "*_w_stat"):
                stat_rw_files[os.path.join(path, name)] = stat_to_df(os.path.join(path, name))
                # print(os.path.join(path, name))

    sd = SankeyData()
    # read workflow.yml 
    with open(os.getcwd()+"/../../workflow.yml") as f:
        try:
            data = yaml.load(f, Loader=yaml.FullLoader)
        except yaml.YAMLError as e:
            print(e)

    jobs = data['jobs']
    for job in jobs:
        inoutputs = job['uses']
        jobname = job['name']
        jobid = job['id']
        for inoutput in inoutputs:
            fname = inoutput['lfn']
            type = inoutput['type']
            if type == 'input':
                statFileName = os.getcwd()+"/"+jobname+"_"+jobid+"/"+fname+"_r_stat"
                rw = 'r'
            elif type == 'output':
                statFileName = os.getcwd()+"/"+jobname+"_"+jobid+"/"+fname+"_w_stat"
                rw = 'w'
            else: 
                print("Error: incorrect access mode to input/output files")
                sys.exit()
            df = stat_rw_files[statFileName]
            sd.load_stat(df, 
                        {'filename': fname,
                         'type': rw,
                         'size': 0,
                         'task_name': jobname+"_"+jobid})

    # sd plot
    sd.build_links()
    sd.nodes
    sd.links
    sd.plot()
