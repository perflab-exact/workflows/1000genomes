# generate shell execution script from Pegasus generated yaml logs
# input: replicas.yml, workflow.yml, transformations.yml
# output: a shell execution script
# usage: create an empty dir, run this script within this dir
# specify the dir where the input yml files located and tazer install
# DIR_TO_YML="/people/guol678/projects/1000genome-workflow/"
# TAZER_INSTALL="/people/guol678/software/tazer/"
# run the generated shell script in this dir

# specify the dir where the input yml files located
DIR_TO_YML="/people/guol678/projects/1000genome-workflow/"
TAZER_INSTALL="/people/guol678/software/tazer/"

import yaml
import os

def gather_input():
    with open(DIR_TO_YML+"replicas.yml") as f:
        try:
            data = yaml.load(f, Loader=yaml.FullLoader)
        except yaml.YAMLError as e:
            print(e)

    replicas = data['replicas']
    with open("gen_exe_script.sh", "w") as f:
        for replica in replicas:
            pfn=replica['pfns'][0]['pfn']
            f.write("cp -r "+pfn+" ./"+"\n")

    f.close()

def generate_exe_commands():
    with open(DIR_TO_YML+"workflow.yml") as f:
        try:
            worflow = yaml.load(f, Loader=yaml.FullLoader)
        except yaml.YAMLError as e:
            print(e)

    jobspfn = {}
    with open(DIR_TO_YML+"transformations.yml") as f:
        try:
            transf = yaml.load(f, Loader=yaml.FullLoader)
        except yaml.YAMLError as e:
            print(e)
    trans = transf['transformations']
    for tran in trans:
        jname = tran['name']
        pjname = tran['sites'][0]['pfn']
        jobspfn[jname] = pjname

    # create dir for Tazer stat files for each workflow
    jobs = worflow['jobs']
    with open("gen_exe_script.sh", "a") as f:
        for job in jobs:
            jobname = job['name']
            jobid = job['id']
            dir_name = jobname+'_'+jobid
            args = job['arguments']
            f.write("mkdir "+dir_name+"\n")
            f.write("LD_PRELOAD="+TAZER_INSTALL+"/lib/libclient.so "+ jobspfn[jobname]+ " ")
            for arg in args:
                f.write(arg + " ")
            f.write("\n")
            f.write("mv *stat " + dir_name + "\n")
            f.write("\n")
    f.close()

if __name__ == "__main__":
    gather_input()
    generate_exe_commands()
